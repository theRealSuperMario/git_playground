


# Cleaning up dirty commit histories using rebase

* Starting from branch `feature/dirty_hack`, clean up the commit history so that
    * Features 1, ..., 3 are grouped in branch `feature/dirty_hack_cleanup_1`and sorted ascending in order.
    * Features 4, ..., 6 are grouped in branch `feature/dirty_hack_cleanup_2`and sorted ascending in order.
    * feature branch `feature/dirty_hack_cleanup_2` is built on top of `feature/dirty_hack_cleanup_1`

1. Steps to rebase into `feature/dirty_hack_cleanup_1`.

```bash
# checkout new cleanup branch from dirty hack branch
git checkout -b feature/dirty_hack_cleanup_1 feature/dirty_hack
# start interactive rebase
git rebase -i master
# drop feature 4, 5, 6
# move `feature 1 V2` after `feature 1`
# squash `feature 1 V2`
```

![alt text](rebase/cleanup_1.jpg "Cleanup 1")


2. Steps to rebase into `feature/dirty_hack_cleanup_2`.

```bash
# checkout new cleanup branch from dirty hack branch
git checkout -b feature/dirty_hack_cleanup_2 feature/dirty_hack
# start interactive rebase
git rebase -i feature/dirty_hack_cleanup_1
# drop `feature 1` (feature 1 is suggested because squash added commit id)
# drop `feature 1 V2` (feature 1 is suggested because squash added commit id)
```

![alt text](rebase/cleanup_2.jpg "Cleanup 2")

3. Finally, observe that `git diff feature/dirty_hack` is empty, hence everything has been cleaned up.

4. Notes:
* Avoid using squash before not being absolutely sure about the commit granularity, as it changes the commit.

* References:
* https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History